package com.devcamp.customerinvoiceapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customerinvoiceapi.model.Invoice;
import com.devcamp.customerinvoiceapi.service.InvoiceService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class InvoiceController {
    @Autowired
    private InvoiceService invoiceService;

    @GetMapping("/invoices")
    public ArrayList<Invoice> getInvoices() {
        ArrayList<Invoice> allInvoice = invoiceService.getAllInvoices();
        return allInvoice;
    }

    @GetMapping("/invoices/{invoiceId}")
    public Invoice getInvoiceById(@PathVariable int invoiceId) {
        return invoiceService.getInvoiceByIndex(invoiceId);
    }
}
