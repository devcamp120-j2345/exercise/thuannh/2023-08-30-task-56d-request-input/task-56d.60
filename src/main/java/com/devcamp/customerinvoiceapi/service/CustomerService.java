package com.devcamp.customerinvoiceapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customerinvoiceapi.model.Customer;

@Service
public class CustomerService {
    Customer customer1 = new Customer(1, "John", 10);
    Customer customer2 = new Customer(2, "Mary", 20);
    Customer customer3 = new Customer(3, "Peter", 5);
    Customer customer4 = new Customer(4, "Frank", 15);
    Customer customer5 = new Customer(5, "Paul", 30);
    Customer customer6 = new Customer(6, "Hellen", 12);
    public ArrayList<Customer> getAllCustomers() {
        ArrayList<Customer> allCustomer = new ArrayList<>();

        allCustomer.add(customer1);
        allCustomer.add(customer2);
        allCustomer.add(customer3);
        allCustomer.add(customer4);
        allCustomer.add(customer5);
        allCustomer.add(customer6);

        return allCustomer;
    }
}
