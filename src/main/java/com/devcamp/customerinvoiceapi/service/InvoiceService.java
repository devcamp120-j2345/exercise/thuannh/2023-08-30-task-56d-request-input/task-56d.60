package com.devcamp.customerinvoiceapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.customerinvoiceapi.model.Invoice;

@Service
public class InvoiceService {
    @Autowired
    private CustomerService customerService;

    Invoice invoice1 = new Invoice(1, 100.0);
    Invoice invoice2 = new Invoice(2, 200.0);
    Invoice invoice3 = new Invoice(3, 300.0);
    Invoice invoice4 = new Invoice(4, 400.0);
    Invoice invoice5 = new Invoice(5, 500.0);
    Invoice invoice6 = new Invoice(6, 600.0);

    public ArrayList<Invoice> getAllInvoices() {
        ArrayList<Invoice> allInvoice = new ArrayList<>();

        invoice1.setCustomer(customerService.getAllCustomers().get(0));
        invoice2.setCustomer(customerService.getAllCustomers().get(1));
        invoice3.setCustomer(customerService.getAllCustomers().get(2));
        invoice4.setCustomer(customerService.getAllCustomers().get(3));
        invoice5.setCustomer(customerService.getAllCustomers().get(4));
        invoice6.setCustomer(customerService.getAllCustomers().get(5));

        allInvoice.add(invoice1);
        allInvoice.add(invoice2);
        allInvoice.add(invoice3);
        allInvoice.add(invoice4);
        allInvoice.add(invoice5);
        allInvoice.add(invoice6);

        return allInvoice;
    }

    public Invoice getInvoiceByIndex(int index){

        ArrayList<Invoice> listInvoices = getAllInvoices();

        if(index < 0 || index > listInvoices.size()){
            return null;
        }
        return listInvoices.get(index);
    }
}
